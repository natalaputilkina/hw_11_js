/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

Event - це сигнал того, що щось сталося на сторинці, використовуються для взаємодії сайту з користувачем.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
click - коли миша клікає на елемент
contextmenu - коли користувач правою кнопкою миші клікає на
елемент
mouseover / mouseout - коли курсор миші наводиться /
залишається на елементі
mousedown / mouseup - коли кнопка миші натискана /
відпускається над елементом
mousemove - коли миша рухається

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
"contextmenu" - це компонент, який виглядає як спливаюче меню, що з'являється на кліку на праву клавішу миші.

 */

// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const buttonClickMe = document.getElementById('btn-click');
const sectionContent = document.getElementById('content');

function eventNewParagraph (){
    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'New Paragraph';
    sectionContent.before(newParagraph);
}

buttonClickMe.addEventListener('click', eventNewParagraph);
 
//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//     По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

function newButton () {
    const buttonInput = document.createElement('button');
    buttonInput.textContent = 'Input create';
    buttonInput.getAttribute('id');
    const buttonInputId = 'btn-input-create';
    buttonInput.setAttribute('id', buttonInputId);
    sectionContent.append(buttonInput);
}

newButton ()

const newButtonInput = document.getElementById('btn-input-create')

function newElementInput(){
    const newInput = document.createElement('input');
    const name = 'name'
    newInput.getAttribute('name')
    newInput.setAttribute('name',name)
    newButtonInput.after(newInput)
    
}

newButtonInput.addEventListener('click', newElementInput)

 


